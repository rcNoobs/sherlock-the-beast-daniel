﻿using System;
using System.Diagnostics;

namespace SBDaniel
{
    class Program
    {
        static void Main()
        {
            int t = 100000;

            var sw = Stopwatch.StartNew();
            for (int i = 1; i <= t; i++)
            {
                HandleNumberInput(i);
            }
            sw.Stop();
            Console.WriteLine($"{sw.ElapsedMilliseconds} ms");
            Console.ReadLine();
        }

        private static void HandleNumberInput(int n)
        {
            var m = n;
            var result = "-1";

            if (m == 3)
            {
                result = concat(m, 0);
            }
            else if (m == 5)
            {
                result = concat(0, m);
            }
            else
            {
                while (m > 0)
                {
                    var max = maxDecentOfFive(m);
                    var rest = n - max;

                    if (max == 3 && rest < 5)
                    {
                        result = "-1";
                        break;
                    }

                    if (rest % 5 == 0)
                    {
                        result = concat(max, rest);
                        break;
                    }
                    m--;
                }
            }
            //Console.WriteLine(result);
        }
        static int maxDecentOfFive(int n)
        {
            while (n >= 0)
            {
                if (n % 3 == 0) return n;
                n--;
            }
            return -1;
        }

        static string concat(int f, int t)
        {
            var result = string.Empty.PadLeft(f, '5');
            result += string.Empty.PadLeft(t, '3');
            return result;
        }
    }
}
